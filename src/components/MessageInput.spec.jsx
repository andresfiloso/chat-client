/**
 * @jest-environment jsdom
 */
import React from "react";
import { render, screen } from "@testing-library/react";
import MessageInput from "./MessageInput";

test("MessageInput", () => {
  const { container } = render(<MessageInput />);
  expect(container.firstChild.classList.contains("box")).toBe(true);
  expect(screen.getByTestId("messageInput"));
});
