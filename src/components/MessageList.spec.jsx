/**
 * @jest-environment jsdom
 */
import React from "react";
import { render, screen } from "@testing-library/react";
import MessageList from "./MessageList";

test("MessageList", () => {
  const user = {
    name: "joe",
  };
  const messages = [
    {
      id: "2",
      from: "fake_id",
      text: "fake_test",
    },
  ];

  const { container } = render(<MessageList user={user} messages={messages} />);
  expect(container.firstChild.classList.contains("box")).toBe(true);
  expect(screen.getByTestId("messages"));
});
