/**
 * @jest-environment jsdom
 */
import React from "react";
import { render, screen } from "@testing-library/react";
import NavBar from "./NavBar";

test("NavBar has logout button", () => {
  render(<NavBar />);
  expect(
    screen.getByRole("button", {
      name: /Logout/i,
    })
  );
});
