module.exports = {
  rootDir: "./",
  testEnvironment: "node",
  testPathIgnorePatterns: ["/node_modules/"],
  testRegex: ".spec.jsx$",
  verbose: true,
};
